# Visualforce CSV Upload Page

## Overview

This page parses the file that the user updloads of acceptable formats. It inserts/updates records from the file, saves the uploaded file to the user's files, and lists the number of/info for effected records. There is also a flow in place to prevent new Benefit records with overlapping dates for Employees.

## Demo

[See the demo video here.](https://www.youtube.com/watch?v=WYNt3t_znw8)

### Screenshots

After user has uploaded their donation file:

![Upload Result](media/uploadResults.png)

File was saved:

![File Saved](media/fileSaved.png)

Employee 401K Flow:

![Benefit Before Save Flow](media/benefitFlow.png)
