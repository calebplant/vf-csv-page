@isTest
public with sharing class CsvPage_Test {

    private static String EXISTING_CON_PHONE = '703-588-1190';
    private static String EXISTING_CON_EMAIL = 'user5@gmail.com';
    private static String EXISTING_ACC_PHONE = '201-511-2070';
    private static String EMPLOYEE_ONE_NAME = 'EmployeeOne';
    
    @TestSetup
    static void makeData(){
        // Accounts
        List<Account> newAccs = new List<Account>();
        Account acc1 = new Account();
        acc1.Name = 'Network For Good';
        acc1.Phone = EXISTING_ACC_PHONE;
        acc1.ShippingStreet = '123 Main St';
        newAccs.add(acc1);
        // Contacts
        List<Contact> newCons = new List<Contact>();
        Contact con1 = new Contact();
        con1.FirstName = 'Kent';
        con1.LastName = 'Katz';
        con1.Email = EXISTING_CON_EMAIL;
        con1.Phone = EXISTING_CON_PHONE;
        con1.MailingStreet = '327 Davis Street';
        newCons.add(con1);
        insert newCons;
        // Employees
        List<Employee__c> newEmps = new List<Employee__c>();
        newEmps.add(new Employee__c(Name = EMPLOYEE_ONE_NAME));
        insert newEmps;
        
    }

    @isTest
    static void doesInsertNewRecordsInsertNewDonationRecords()
    {
        Map<Schema.SObjectType, List<SObject>> recordMaps = new Map<Schema.SObjectType, List<SObject>>();
        // Contacts
        List<SObject> newCons = new List<SObject>();
        Contact con2 = new Contact(LastName = 'Trout');
        newCons.add(con2);
        recordMaps.put(Contact.sObjectType, newCons);
        // My Custom Objects
        List<SObject> newCustObjs = new List<SObject>();
        My_Custom_Object__c custObj1 = new My_Custom_Object__c(Name='Skipper');
        newCustObjs.add(custObj1);
        recordMaps.put(My_Custom_Object__c.sObjectType, newCustObjs);

        CsvPageController myController = new CsvPageController();

        Test.startTest();
        Integer numOfInserts = myController.insertNewRecords(recordMaps);
        Test.stopTest();

        Contact outCon = [SELECT Id FROM Contact WHERE LastName = 'Trout'];
        My_Custom_Object__c custObj = [SELECT Id FROM My_Custom_Object__c WHERE Name = 'Skipper'];
        System.assertNotEquals(null, outCon);
        System.assertNotEquals(null, custObj);
        System.assertEquals(2, numOfInserts);
    }

    @isTest
    static void doesInsertNewRecordsUpdateExistingDonationRecords()
    {
        Map<Schema.SObjectType, List<SObject>> recordMaps = new Map<Schema.SObjectType, List<SObject>>();
        // Contacts
        List<SObject> newCons = new List<SObject>();
        Contact con2 = new Contact(LastName = 'Trout', Email = EXISTING_CON_EMAIL, Phone = EXISTING_CON_PHONE);
        newCons.add(con2);
        recordMaps.put(Contact.sObjectType, newCons);
        // Accounts
        List<SObject> newAccs = new List<SObject>();
        Account acc2 = new Account(Name='Skipper');
        newAccs.add(acc2);
        Account acc3 = new Account(Name = 'Banner', Phone = EXISTING_ACC_PHONE);
        newAccs.add(acc3);
        recordMaps.put(Account.sObjectType, newAccs);

        CsvPageController myController = new CsvPageController();

        Test.startTest();
        Integer numOfInserts = myController.insertNewRecords(recordMaps);
        Test.stopTest();

        Contact outCon = [SELECT Id, LastName FROM Contact WHERE Email = :EXISTING_CON_EMAIL AND Phone = :EXISTING_CON_PHONE];
        Account outAcc = [SELECT Id, Name FROM Account WHERE Phone = :EXISTING_ACC_PHONE];
        System.assertEquals('Trout', outCon.LastName);
        System.assertEquals('Banner', outAcc.Name);
        System.assertEquals(3, numOfInserts);
    }

    @isTest
    static void doesBenefitFlowPreventDuplicate401KDateRanges()
    {
        Employee__c emp = [SELECT Id FROM Employee__c WHERE Name = :EMPLOYEE_ONE_NAME];
        Benefit__c startBenefit = new Benefit__c(Employee__c = emp.Id, Type__c = '401K');
        startBenefit.Start_Date__c = System.today();
        startBenefit.End_Date__c = System.today().addDays(1);
        insert startBenefit;

        // Set up benefits for insert
        List<Benefit__c> benefits = new List<Benefit__c>();
        // 401K before with no overlap
        Benefit__c validBefore = new Benefit__c(Employee__c = emp.Id, Type__c = '401K');
        validBefore.Start_Date__c = System.today().addDays(-2);
        validBefore.End_Date__c = System.today().addDays(-1);
        benefits.add(validBefore);
        // 401K after with no overlap
        Benefit__c validAfter = new Benefit__c(Employee__c = emp.Id, Type__c = '401K');
        validAfter.Start_Date__c = System.today().addDays(2);
        validAfter.End_Date__c = System.today().addDays(3);
        benefits.add(validAfter);
        // 401K before with overlap
        Benefit__c overlapBefore = new Benefit__c(Employee__c = emp.Id, Type__c = '401K');
        overlapBefore.Start_Date__c = System.today().addDays(-2);
        overlapBefore.End_Date__c = System.today();
        benefits.add(overlapBefore);
        // 401K after with overlap
        Benefit__c overlapAfter = new Benefit__c(Employee__c = emp.Id, Type__c = '401K');
        overlapAfter.Start_Date__c = System.today();
        overlapAfter.End_Date__c = System.today().addDays(3);
        benefits.add(overlapAfter);
        // 401K before encompassing
        Benefit__c overlapEncompassing = new Benefit__c(Employee__c = emp.Id, Type__c = '401K');
        overlapEncompassing.Start_Date__c = System.today().addDays(-3);
        overlapEncompassing.End_Date__c = System.today().addDays(3);
        benefits.add(overlapEncompassing);

        Test.startTest();
        Database.insert(benefits, false);
        Test.stopTest();

        System.assertNotEquals(null, validBefore.Id);
        System.assertNotEquals(null, validAfter.Id);
        System.assertEquals(null, overlapBefore.Id);
        System.assertEquals(null, overlapAfter.Id);
        System.assertEquals(null, overlapEncompassing.Id);
    }

    @isTest
    static void doesParseFilesReturnParsedDonationFile()
    {
        String fileContent = 'Donation Id,Donation Date,Donation Status,Donation Amount,Donation Frequency,Donor First Name,Donor Last Name,Donor Email Address,Donor Phone,Designation,Donor Address 1,Donor Address 2,Donor City,Donor State,Donor Zip,Donor Country,Amount  Received,Source,Source Detail,Check Number,How did you hear about Childrens Future?,"If you learned of this organization through a friend, family member, or colleague please enter his/her name:"\n'
        + '1209174248,10/8/2014,Successful,$60.00 ,Monthly,Jason,Brimhall,user1@gmail.com,Not Shared,I want to be a monthly sponsor,Not Shared,Not Shared,Not Shared,Not Shared,Not Shared,Not Shared,$60.00 ,Network for Good,,,,';
        
        Test.startTest();
        Map<Schema.SObjectType, List<SObject>> recordMaps = ParsingService.parseFile(fileContent);
        Test.stopTest();

        for(List<SObject> eachList : recordMaps.values()) {
            for(SObject eachObj : eachList) {
                System.debug(eachObj);
            }
        }
        System.assertEquals(1, recordMaps.get(Account.SObjectType).size());
        System.assertEquals(1, recordMaps.get(Contact.SObjectType).size());
        // System.assertEquals(1, recordMaps.get(Opportunity.SObjectType).size());
    }
}
