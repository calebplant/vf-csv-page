public with sharing class ParsingMaps {
    private static Map<String, Integer> myCustomObjectMap = new Map<String, Integer> {
        'Name' => 0,
        'Street' => 1,
        'City' => 2,
        'State' => 3,
        'PostalCode' => 4,
        'Phone' => 5
    };

    public static Integer getIndex(String mapType, String keyName)
    {
        switch on mapType {
            when 'My_Custom_Object__c' {
                return myCustomObjectMap.get(keyName);
            } when else {
                return 0;
            }
        }
    }

    public class DonationObjects {
        private Map<String, Integer> contactMap = new Map<String, Integer> {
            'FirstName' => 5,
            'LastName' => 6,
            'Email' => 7,
            'Phone' => 8,
            'MailingStreet' => 10,
            'OtherStreet' => 11,
            'MailingCity' => 12,
            'MailingState' => 13,
            'MailingPostalCode' => 14,
            'MailingCountry' => 15,
            'Description' => 9
        };

        private Map<String, Integer> accountMap = new Map<String, Integer> {
            'Name' => 17,
            // 'Email' => 7,
            'Phone' => 8,
            'ShippingStreet' => 10,
            // 'OtherStreet' => 11,
            'ShippingCity' => 12,
            'ShippingState' => 13,
            'ShippingPostalCode' => 14,
            'ShippingCountry' => 15,
            'Description' => 9
        };

        private Map<String, Integer> opportunityMap = new Map<String, Integer> {
            // 'ContactFirstName' => 5,
            // 'ContactLastName' => 6,
            'AccountName' => 17,
            'CloseDate' => 1,
            'Amount' => 3,
            'Description' => 9,
            'StageName' => 2
            // Lead source?
        };

        public Integer getIndex(String mapType, String keyName)
        {
            switch on mapType {
                when 'Contact' {
                    return contactMap.get(keyName);
                } when 'Account' {
                    return accountMap.get(keyName);
                } when 'Opportunity' {
                    return opportunityMap.get(keyName);
                } when else {
                    return 0;
                }
            }
        }
    }
}
