public with sharing class CsvPageController {
    public Blob file { get; set; }
    public String fName {get; set;}
    public Boolean csvParsed {get; set;}
    public Integer numOfNewRecords {get; set;}

    public Map<Integer, String> addedRecordLabels {get; set;}
    public Map<Integer, List<sObject>> addedRecords {get; set;}
    public Set<Integer> addedRecordKeys {get; set;}
    public Map<Integer, List<String>> addedRecordColumns{get; set;}

    private Map<String, List<String>> displayedColumnsByPluralLabel = new Map<String, List<String>>{
        'Contacts' => new List<String> {'FirstName', 'LastName', 'Email', 'Phone'},
        'Accounts' => new List<String> {'Name', 'Phone'},
        'Opportunities' => new List<String> {'Name', 'Amount', 'CloseDate', 'StageName'},
        'My Custom Objects' => new List<String> {'Name', 'Street__c', 'Phone__c'}
    };

    public CsvPageController() {
        numOfNewRecords = 0;
        addedRecordLabels = new Map<Integer, String>();
        addedRecords = new Map<Integer, List<sObject>>();
        addedRecordKeys = new Set<Integer>();
        addedRecordColumns = new Map<Integer, List<String>>();
    }

    public PageReference uploadFile() {
        System.debug('START uploadFile');

        String fileContent = file.toString();
        Map<Schema.SObjectType, List<sObject>> parseResults = ParsingService.parseFile(fileContent);
        numOfNewRecords = insertNewRecords(parseResults);

        // for(List<SObject> eachList : parseResults.values()) {
        //     System.debug(eachList);
        // }
        csvParsed = true;
        saveFile();

        return null;
    }

    @TestVisible
    private Integer insertNewRecords(Map<Schema.SObjectType, List<SObject>> recordMaps)
    {

        Integer numOfInserts = 0;
        Integer addedRecordTypeIndex = 0;

        // for(Sobject eachObj : recordMaps.get(Opportunity.sObjectType)) {
        //     System.debug(eachObj);
        // }

        // /*
        for(List<SObject> eachList : recordMaps.values()) {
            for(SObject eachObj : eachList) {
                System.debug(eachObj);
            }
            List<SObject> successfulRecords = new List<SObject>();
            
            
            // List<Database.SaveResult> saveResults = Database.insert(eachList, false);
            List<Database.UpsertResult> saveResults = Database.upsert(eachList, false);
            for(Database.UpsertResult eachSr : saveResults) {
                if(eachSr.isSuccess()) {
                    numOfInserts++;
                } else {
                    for(Database.Error err : eachSr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                    }
                }
            }
            

            for(SObject eachRecord : eachList) {
                if(eachRecord.Id != null) {
                    successfulRecords.add(eachRecord);
                }
            }

            if(successfulRecords.size() > 0) {
                System.debug('successfulRecords: ' + successfulRecords);
                DescribeSObjectResult  objDesc = successfulRecords[0].Id.getSobjectType().getDescribe();
                System.debug('objDesc: ' + objDesc);
                String pluralLabel = objDesc.getLabelPlural();
                System.debug('label: ' + pluralLabel);

                addedRecordLabels.put(addedRecordTypeIndex, pluralLabel);
                addedRecords.put(addedRecordTypeIndex, successfulRecords);
                addedRecordColumns.put(addedRecordTypeIndex, displayedColumnsByPluralLabel.get(pluralLabel));

                addedRecordTypeIndex++;
            }
        }
        addedRecordKeys = addedRecordLabels.keySet();
        // */
        
        return numOfInserts;
    }

    private Map<Schema.SObjectType, List<SObject>> populateExistingIds(Map<Schema.SObjectType, List<SObject>> recordMap)
    {
        Map<Schema.SObjectType, List<SObject>> result = new Map<Schema.SObjectType, List<SObject>>();

        for(Schema.SObjectType eachType : recordMap.keySet()) {

            // Accounts
            if(eachType == Account.SobjectType) { 
                Map<String, Id> existingAccIdByPhone = mapExistingAccByPhone(recordMap.get(eachType));
                System.debug('existingAccIdByPhone: ' + existingAccIdByPhone);
                for(sObject eachNewAcc : recordMap.get(eachType)) {
                    if((existingAccIdByPhone.get((String)eachNewAcc.get('Phone'))) != null) {
                        eachNewAcc.put('Id', existingAccIdByPhone.get((String)eachNewAcc.get('Phone'))); // populate Account Id if already exists
                    }
                }
            }

            // Contacts
            else if (eachType == Contact.SobjectType) {
                Map<String, Id> existingConIdByPhone = mapExistingConByPhone(recordMap.get(eachType));
                Map<String, Id> existingConIdByEmail = mapExistingConByEmail(recordMap.get(eachType));
                System.debug('existingConIdByPhone: ' + existingConIdByPhone);
                System.debug('existingConIdByEmail: ' + existingConIdByEmail);
                for(sObject eachNewCon : recordMap.get(eachType)) {
                    if((existingConIdByPhone.get((String)eachNewCon.get('Phone')) != null && existingConIdByEmail.get((String)eachNewCon.get('Email')) != null)) {
                        Id phoneId = existingConIdByPhone.get((String)eachNewCon.get('Phone'));
                        Id emailId = existingConIdByEmail.get((String)eachNewCon.get('Email'));
                        if(phoneId == emailId) {
                            eachNewCon.put('Id', existingConIdByPhone.get((String)eachNewCon.get('Phone'))); // populate Contact Id if already exists 
                        }
                    }
                }
            }

            result.put(eachType, recordMap.get(eachType));
        }
        return result;
    }

    private Map<String, Id> mapExistingAccByPhone(List<Account> newAccs)
    {
        // Get list of existing Accounts with given Phone value
        Map<String, Id> existingAccIdByPhone = new Map<String, Id>();
        List<String> newAccPhones = new List<String>();
        for(Account eachAcc : newAccs) {
            if(eachAcc.Phone != null && !String.isBlank(eachAcc.Phone)) {
                newAccPhones.add(eachAcc.Phone);
            }
        }
        System.debug('newAccPhones: ' + newAccPhones);
        List<Account> accsWithExistingPhone = [SELECT Id, Phone FROM Account WHERE Phone IN :newAccPhones];
        System.debug('accsWithExistingPhone: ' + accsWithExistingPhone);
        for(Account eachExistingAcc : accsWithExistingPhone) {
            existingAccIdByPhone.put(eachExistingAcc.Phone, eachExistingAcc.Id);
        }
        return existingAccIdByPhone;
    }

    private Map<String, Id> mapExistingConByPhone(List<Contact> newCons)
    {
        // Get list of existing Contacts with given Phone value
        Map<String, Id> existingConIdByPhone = new Map<String, Id>();
        List<String> newConPhones = new List<String>();
        for(Contact eachCon : newCons) {
            if(eachCon.Phone != null && !String.isBlank(eachCon.Phone)) {
                newConPhones.add(eachCon.Phone);
            }
        }
        System.debug('newConPhones: ' + newConPhones);
        List<Contact> consWithExistingPhone = [SELECT Id, Phone FROM Contact WHERE Phone IN :newConPhones];
        System.debug('consWithExistingPhone: ' + consWithExistingPhone);
        for(Contact eachExistingCon : consWithExistingPhone) {
            existingConIdByPhone.put(eachExistingCon.Phone, eachExistingCon.Id);
        }
        return existingConIdByPhone;
    }

    private Map<String, Id> mapExistingConByEmail(List<Contact> newCons)
    {
        // Get list of existing Contacts with given Phone value
        Map<String, Id> existingConIdByEmail = new Map<String, Id>();
        List<String> newConEmails = new List<String>();
        for(Contact eachCon : newCons) {
            if(eachCon.Email != null && !String.isBlank(eachCon.Email)) {
                newConEmails.add(eachCon.Email);
            }
        }
        System.debug('newConEmails: ' + newConEmails);
        List<Contact> consWithExistingEmail = [SELECT Id, Email FROM Contact WHERE Email IN :newConEmails];
        System.debug('consWithExistingEmail: ' + consWithExistingEmail);
        for(Contact eachExistingCon : consWithExistingEmail) {
            existingConIdByEmail.put(eachExistingCon.Email, eachExistingCon.Id);
        }
        return existingConIdByEmail;
    }

    private void saveFile()
    {
        ContentVersion v = new ContentVersion();
        v.versionData = file;
        v.title = fName;
        v.pathOnClient ='/' + fName;
        insert v;
    }
}
