public with sharing class ParsingService {
    
    // private Enum MY_CUSTOM_OBJECT {NAME, STREET, CITY, STATE, POSTAL_CODE, PHONE}
    private static String START_OF_MY_CUSTOM_OBJECT = 'Name';
    private static String START_OF_DONATION_FILE = 'DonationId';
    private static String CON_LINE_NAME = 'Contact';
    private static String ACC_LINE_NAME = 'Account';
    private static String OPP_LINE_NAME = 'Opportunity';
    private static String CUST_OBJ_NAME = 'My_Custom_Object__c';
    private static Map<String, String> OPP_STAGE_NAME_TRANSLATION = new Map<String, String> {
        'Successful' => 'Closed'
    };

    public static Map<Schema.SObjectType, List<SObject>> parseFile(String fileContent)
    {
        System.debug('START parseFile');
        String firstCellContent = fileContent.split(',')[0].replaceAll('[^a-zA-Z0-9]', '');
        Map<Schema.SObjectType, List<SObject>> result = new Map<Schema.SObjectType, List<SObject>>();

        if(firstCellContent.startsWith(START_OF_MY_CUSTOM_OBJECT)) {
            System.debug('Parse My Custom Objects');
            result.put(My_Custom_Object__c.sObjectType, parseMyCustomObjects(fileContent));
        } else if (firstCellContent.startsWith(START_OF_DONATION_FILE)) {
            System.debug('Parse Donations');
            result = parseDonationFile(fileContent);
        }

        return result;
    }

    public static List<My_Custom_Object__c> parseMyCustomObjects(String fileContent)
    {
        List<String> fileLines = fileContent.split('\n');

        // System.debug(fileLines);
        List<My_Custom_Object__c> newCustomObjs = new List<My_Custom_Object__c>();
        for(Integer n=1; n < fileLines.size(); n++){
            List<String> lineData = fileLines[n].split(',');
            // System.debug(lineData);

            My_Custom_Object__c newObj = new My_Custom_Object__c();
            newObj.Name = lineData[ParsingMaps.getIndex(CUST_OBJ_NAME, 'Name')];
            newObj.Street__c = lineData[ParsingMaps.getIndex(CUST_OBJ_NAME, 'Street')];
            newObj.City__c = lineData[ParsingMaps.getIndex(CUST_OBJ_NAME, 'City')];
            newObj.State__c = lineData[ParsingMaps.getIndex(CUST_OBJ_NAME, 'State')];
            newObj.Postal_Code__c = lineData[ParsingMaps.getIndex(CUST_OBJ_NAME, 'PostalCode')];
            newObj.Phone__c = lineData[ParsingMaps.getIndex(CUST_OBJ_NAME, 'Phone')];
            newCustomObjs.add(newObj);
        }
        return newCustomObjs;
    }

    private static Map<Schema.SObjectType, List<SObject>> parseDonationFile(String fileContent)
    {
        Map<Schema.SObjectType, List<SObject>> result = new Map<Schema.SObjectType, List<SObject>>();
        List<Contact> newCons = new List<Contact>();
        List<Account> newAccs = new List<Account>();
        List<Opportunity> newOpps = new List<Opportunity>();

        List<String> fileLines = fileContent.split('\n');
        for(Integer n=1; n < fileLines.size(); n++){
            List<String> lineData = fileLines[n].split(',');
            newcons.add(parseContact(lineData));
            newAccs.add(parseAccount(lineData));
            newOpps.add(parseOpportunity(lineData));
        }
        result.put(Contact.sObjectType, newCons);
        result.put(Account.sObjectType, newAccs);
        result.put(Opportunity.sObjectType, newOpps);
        return result;
    }

    private static Contact parseContact(List<String> data) {
        Contact newCon = new Contact();
        ParsingMaps.DonationObjects donationParser = new ParsingMaps.DonationObjects();
        newCon.FirstName = data[donationParser.getIndex(CON_LINE_NAME, 'FirstName')];
        newCon.LastName = data[donationParser.getIndex(CON_LINE_NAME, 'LastName')];
        newCon.Email = data[donationParser.getIndex(CON_LINE_NAME, 'Email')];
        if(!(data[donationParser.getIndex(CON_LINE_NAME, 'Phone')] == 'Not Shared')) {
            newCon.Phone = data[donationParser.getIndex(CON_LINE_NAME, 'Phone')];
        }
        newCon.MailingStreet = data[donationParser.getIndex(CON_LINE_NAME, 'MailingStreet')];
        newCon.OtherStreet = data[donationParser.getIndex(CON_LINE_NAME, 'OtherStreet')];
        newCon.MailingCity = data[donationParser.getIndex(CON_LINE_NAME, 'MailingCity')];
        newCon.MailingState = data[donationParser.getIndex(CON_LINE_NAME, 'MailingState')];
        newCon.MailingPostalCode = data[donationParser.getIndex(CON_LINE_NAME, 'MailingPostalCode')];
        newCon.MailingCountry = data[donationParser.getIndex(CON_LINE_NAME, 'MailingCountry')];
        newCon.Description = data[donationParser.getIndex(CON_LINE_NAME, 'Description')];
        return newCon;
    }

    private static Account parseAccount(List<String> data) {
        Account newAcc = new Account();
        ParsingMaps.DonationObjects donationParser = new ParsingMaps.DonationObjects();
        newAcc.Name = data[donationParser.getIndex(ACC_LINE_NAME, 'Name')];
        // newAcc.Email = data[donationParser.getIndex(ACC_LINE_NAME, 'Email')];
        if(!(data[donationParser.getIndex(ACC_LINE_NAME, 'Phone')] == 'Not Shared')) {
            newAcc.Phone = data[donationParser.getIndex(ACC_LINE_NAME, 'Phone')];
        }
        newAcc.BillingStreet = data[donationParser.getIndex(ACC_LINE_NAME, 'ShippingStreet')];
        newAcc.BillingCity = data[donationParser.getIndex(ACC_LINE_NAME, 'ShippingCity')];
        newAcc.BillingState = data[donationParser.getIndex(ACC_LINE_NAME, 'ShippingState')];
        newAcc.BillingPostalCode = data[donationParser.getIndex(ACC_LINE_NAME, 'ShippingPostalCode')];
        newAcc.BillingCountry = data[donationParser.getIndex(ACC_LINE_NAME, 'ShippingCountry')];
        newAcc.Description = data[donationParser.getIndex(ACC_LINE_NAME, 'Description')];
        return newAcc;
    }

    private static Opportunity parseOpportunity(List<String> data) {
        Opportunity newOpp = new Opportunity();
        ParsingMaps.DonationObjects donationParser = new ParsingMaps.DonationObjects();

        newOpp.Name = data[donationParser.getIndex(OPP_LINE_NAME, 'AccountName')] + ' ' + data[donationParser.getIndex(OPP_LINE_NAME, 'CloseDate')]; // Placeholder?
        newOpp.CloseDate = Date.parse(data[donationParser.getIndex(OPP_LINE_NAME, 'CloseDate')]);
        newOpp.Amount = Decimal.valueOf(data[donationParser.getIndex(OPP_LINE_NAME, 'Amount')].replaceAll('[^0-9.]', ''));
        newOpp.Description = data[donationParser.getIndex(OPP_LINE_NAME, 'Description')];
        newOpp.StageName = OPP_STAGE_NAME_TRANSLATION.get(data[donationParser.getIndex(OPP_LINE_NAME, 'StageName')]);
        return newOpp;
    }
}
